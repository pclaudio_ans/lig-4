//referência container
const container = document.getElementById("container");

//referência modal
let modal = document.getElementById("modal");

//referência botão de fechar
let closeButton = document.getElementById("close-btn");

//referência textoVitoria
let text = document.createElement("p");

// referência botao reiniciar
const button = document.getElementById("button");

//variável de controle para alternar as cores dos discos
let lastDiscColor = true;

//referência para container do scoreboard
let scoreRed = document.getElementById('scoreRed');
let scoreGreen = document.getElementById('scoreGreen');

//referência para contagem de pontuação
let counterWinGreen = 0;
let counterWinRed = 0;


//adicione um disco na torre clicada
const addDisc = (e) => {
  let evt = e.currentTarget;

  if (evt.childElementCount < 6) {
    const column = parseInt(evt.id.slice(-1) - 1);
    const row = evt.childElementCount;
    const color = lastDiscColor ? "vermelho" : "verde";

    player(color);

    const divDisco = document.createElement("div");
    divDisco.classList.add("disco");
    divDisco.classList.add(color);

    evt.appendChild(divDisco);

    addDiscToMap(column, color);

    if (isVictory(column, row, color)) {
      countWin(color);
      uptadeScore(counterWinRed, counterWinGreen);
      
      if(counterWinRed === 3 || counterWinGreen === 3){
        showModalText(`Parabéns, ${color} foi o vencedor!`);
        resetGame();
      } else {
        showModalText (`Parabéns, ${color} venceu a rodada!`);
      }
    } else if (isEven()) {
      showModalText("Empate!!!");

    }

    lastDiscColor = !lastDiscColor;
  }
};

//referência column
const columns = document.querySelectorAll(".columns");
columns.forEach((column) => {
  column.addEventListener("click", addDisc);
});

//mapeamento dos discos
let mapDiscs = [[], [], [], [], [], [], []];

//mapea as posições dos discos
const addDiscToMap = (column, color) => {
  mapDiscs[column].push(color);
};

//verifica verticalmente condição de
const checkVertically = (column, color) => {
  const colors = mapDiscs[column];
  let counter = 0;

  for (let i = 0; i < colors.length; i++) {
    if (mapDiscs[column][i] === color) {
      counter++;
    } else {
      counter = 0;
    }

    if (counter === 4) {
      return true;
    }
  }

  return false;
};

//verifica horizontalmente condição de vitória
const checkHorizontally = (row, color) => {
  let counter = 0;
  for (let i = 0; i < 7; i++) {
    if (mapDiscs[i][row] === color) {
      counter++;
    } else {
      counter = 0;
    }

    if (counter === 4) {
      return true;
    }
  }

  return false;
};

//verifica diagonalmente ascendente condição de vitória
const checkDiagonallyAsc = (column, row, color) => {
  let counter = 0;
  let currentColumn = column;
  let currentRow = row;

  for (let i = 0; i < 3; i++) {
    if (--currentColumn >= 0 && --currentRow >= 0) {
      if (mapDiscs[currentColumn][currentRow] === color) {
        counter++;
      } else {
        break;
      }
    }
  };

  currentColumn = column;
  currentRow = row;

  for (let i = counter; i < 3; i++) {
    if (++currentColumn <= 6 && ++currentRow <= 5) {
      if (mapDiscs[currentColumn][currentRow] === color) {
        counter++;
      } else {
        break;
      }
    }
  }

  return counter === 3;
};

//verifica diagonalmente descendente condição de vitória
const checkDiagonallyDesc = (column, row, color) => {
  let counter = 0;
  let currentColumn = column;
  let currentRow = row;

  for (let i = 0; i < 3; i++) {
    if (++currentColumn <= 6 && --currentRow >= 0) {
      if (mapDiscs[currentColumn][currentRow] === color) {
        counter++;
      } else {
        break;
      }
    }
  }

  currentColumn = column;
  currentRow = row;

  for (let i = counter; i < 3; i++) {
    if (--currentColumn >= 0 && ++currentRow <= 5) {
      if (mapDiscs[currentColumn][currentRow] === color) {
        counter++;
      } else {
        break;
      }
    }
  }

  return counter === 3;
};

//verifica todas as condições de virtória
const isVictory = (column, row, color) => {
  if (
    checkVertically(column, color) ||
    checkHorizontally(row, color) ||
    checkDiagonallyAsc(column, row, color) ||
    checkDiagonallyDesc(column, row, color)
  ) {
    return true;
  }
  return false;
};

//verifica a condição de empate
const isEven = () => {
  return mapDiscs.flat().length === 42;
};

//função faz o modal do vencedor aparecer
const showModalText = (mensagem) => {
  let contentModal = document.getElementById("content-modal");

  text.innerHTML = "";

  text.innerText = mensagem;

  contentModal.appendChild(text);

  modal.classList.toggle("hidden");
};

//fechar modal
closeButton.addEventListener("click", function () {
  modal.classList.toggle("hidden");
  resetTable();
});

// função que troca cor do jogador
const player = (color) => {
  const player1 = document.getElementById("player1");
  const player2 = document.getElementById("player2");
  if (color === "verde") {
    player1.classList.add("player1");
    player2.classList.remove("player2");
  } else {
    player1.classList.remove("player1");
    player2.classList.add("player2");
  }
};

player("verde");

const resetGame = () => {
  columns.forEach((column) => {
    column.innerHTML = " ";
  });
  mapDiscs = [[], [], [], [], [], [], []];
  lastDiscColor = true;
  player("verde");

  counterWinGreen = 0;
  counterWinRed = 0;

  uptadeScore(counterWinRed, counterWinGreen);
};

button.addEventListener("click", resetGame);


//função que faz a contagem do placar
const countWin = (color) => {

  if (color === 'vermelho') {
    counterWinRed++
  } else {
    counterWinGreen++
  }
};

const uptadeScore = (red, green) => {
  scoreRed.innerText = red;
  scoreGreen.innerText = green;
};

//função reseta tabuleiro caso empate ou vitória
const resetTable = () => {
  
  columns.forEach((column) => {
    column.innerHTML = " ";
  });
  mapDiscs = [[], [], [], [], [], [], []];
  lastDiscColor = true;
  player("verde");
};


/**
 * 
 * <header>
        <h1>Lig-4 Mario!</h1>
        <i class="fas fa-question-circle fa-3x"></i>
      </header>
      <div id="scoreboard" class="score__style">
        <div id="scoreRed" class="score--style">0</div>
        <p>-</p>
        <div id="scoreGreen" class="score--style">0</div>
      </div>

      <div id="modalTips" class="modalCSS hidden">
        <div id="contentModalTips" class="modal-content">
          <div class="regras">
            <p>Regras do jogo:</p>
            <p>1- Jogador que completar a diagonal, vertical ou horizontal. Marca um ponto!</p>
            <p>2- O jogo começa com o personagem Mário.</p>
          </div>
        </div>
      </div>
 * 
 */